import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./style.css";

class ProductsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      inputValue: "",
      editingIndex: -1,
    };
  }

  addItem = () => {
    const { inputValue } = this.state;
    if (inputValue.trim() === "") {
      const newItem = {
        name: inputValue,
        completed: false,
      };

      this.setState((prevState) => ({
        items: [...prevState.items, newItem],
        inputValue: "",
      }));
    }
  };

  editItem = (index) => {
    const { items } = this.state;
    this.setState({
      editingIndex: index,
      inputValue: items[index].name,
    });
  };

  saveItem = () => {
    const { inputValue, editingIndex, items } = this.state;
    if (inputValue.trim() !== "") {
      const updatedItems = [...items];
      updatedItems[editingIndex].name = inputValue;

      this.setState({
        items: updatedItems,
        inputValue: "",
        editingIndex: -1,
      });
    }
  };

  deleteItem = (index) => {
    const { items } = this.state;
    const updatedItems = [...items];
    updatedItems.splice(index, 1);

    this.setState({
      items: updatedItems,
    });
  };

  toggleComplete = (index) => {
    const { items } = this.state;
    const updatedItems = [...items];
    updatedItems[index].completed = !updatedItems[index].completed;

    this.setState({
      items: updatedItems,
    });
  };

  handleInputChange = (event) => {
    this.setState({
      inputValue: event.target.value,
    });
  };

  render() {
    const { items, inputValue, editingIndex } = this.state;

    return (
      <div>
        <h2>Список покупок</h2>

        <ul>
          {items.map((item, index) => (
            <li key={index} className={item.completed ? "completed" : ""}>
              {index === editingIndex ? (
                <>
                  <input
                    type="text"
                    value={inputValue}
                    onChange={this.handleInputChange}
                  />
                  <button onClick={this.saveItem}>Зберегти 📀</button>
                </>
              ) : (
                <>
                  <span>{item.name}</span>
                  <button onClick={() => this.editItem(index)}>✍️</button>
                  <button onClick={() => this.deleteItem(index)}>❌</button>
                  <button onClick={() => this.toggleComplete(index)}>✅</button>
                </>
              )}
            </li>
          ))}
        </ul>

        <div>
          <button onClick={this.addItem}>Додати ➕</button>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<ProductsList />, document.getElementById("root"));
